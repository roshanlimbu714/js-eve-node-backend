const express = require('express');
const cors = require('cors');
const app = express();

app.use(express.json());
app.use(cors());

const PORT = process.env.PORT || 3030;

const todoItems = [
    { id: 1, title: 'todo 1',
     img: 'https://cdn.pixabay.com/photo/2023/08/05/14/24/twilight-8171206_1280.jpg' },
    { id: 2, title: 'todo 2',
     img: 'https://cdn.pixabay.com/photo/2023/08/11/04/51/fireworks-8182800_640.jpg' },
    { id: 3, title: 'todo 3',
     img: 'https://cdn.pixabay.com/photo/2023/04/05/09/17/spring-7901015_1280.jpg' },
    { id: 4, title: 'todo 4',
     img: 'https://cdn.pixabay.com/photo/2023/11/24/17/22/pigeon-8410348_640.jpg' },
    { id: 5, title: 'todo 5',
     img: 'https://cdn.pixabay.com/photo/2024/02/10/08/45/ramadan-8564369_640.png' },
    { id: 6, title: 'todo 6',
     img: 'https://cdn.pixabay.com/photo/2023/06/10/15/40/berlin-8054311_640.jpg' },
    { id: 7, title: 'todo 7',
     img: 'https://cdn.pixabay.com/photo/2023/03/03/23/20/car-7828554_640.jpg' },
    { id: 8, title: 'todo 8',
     img: 'https://cdn.pixabay.com/photo/2024/01/04/09/34/sick-8486959_640.png' },
    { id: 9, title: 'todo 9',
     img: 'https://cdn.pixabay.com/photo/2023/05/22/06/41/beach-8009994_640.jpg' },
    { id: 10, title: 'todo 10',
     img: 'https://cdn.pixabay.com/photo/2024/01/15/06/40/mallard-8509487_640.jpg' }
]

app.get('/todo', (req, res)=>{
    res.send({
        status:200,
        data: todoItems,
        message:'Todo items retrieved'
    })
});

app.get('/todo/:id', (req, res)=>{
    const task = todoItems.find(v=> v.id === +req.params.id);
    if(task){
    res.send({
        status:200,
        data: task,
        message:'Todo item retrieved'
    })
    }else{
        res.send({
            status:404,
            data: null,
            message:'Todo item retrieve fail'
        })  
    }
});

app.listen(PORT, () => {
    console.log('Server is running on port ' + PORT);
});